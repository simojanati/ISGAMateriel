import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  templateUrl: './materiel.component.html',
  styleUrls: ['./materiel.component.css']
})
export class MaterielComponent implements OnInit {

  public sousMateriel: boolean = false;
  public radio: string = "Non";

  constructor() { }

  ngOnInit() {
  }

  isSousMateriel() {
    setTimeout(() => {
      console.log("radio : " + this.radio);
      if (this.radio === "Oui")
        this.sousMateriel = true;
      else
        this.sousMateriel = false;
    }, 10);
  }

}
