import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public password: string = "";
  public confPassword: string = "";
  public valid: string = "";
  public open: boolean = true;

  constructor() { }

  ngOnInit() {
  }

  openValidation() {
    if (this.password != "")
      this.open = false;
    else
      this.open = true;
  }

  passwordValidation() {
    if (this.password === this.confPassword)
      this.valid = "green";
    else
      this.valid = "red";
  }

}
